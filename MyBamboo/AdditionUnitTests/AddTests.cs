﻿using MyBamboo;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdditionUnitTests
{
    [TestFixture]
    public class AddTests
    {
        [Test]
        public void Test_That_AddTwoNumbers_Method_Returns_Result()
        {
            Addition ad = new Addition();

            Assert.IsTrue(ad.AddTwoNumbers(1,1) == 2);
        }
    }
}
